package com.arenas.claseandroidjava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView tituloSuperiorIzquierdo, todos, titulo, ancho, loremIpsum, fondo;
    EditText nombre, pass, telefono, email;

    Button probar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tituloSuperiorIzquierdo = findViewById(R.id.tvTituloSuperiorIzquierdo);
        todos = findViewById(R.id.tvTodos);
        titulo = findViewById(R.id.tvTitulo);
        ancho = findViewById(R.id.tvAcho);
        loremIpsum = findViewById(R.id.tvLoremIpsum);
        fondo = findViewById(R.id.tvFondo);

        nombre = findViewById(R.id.etNombre);
        pass = findViewById(R.id.etPass);
        telefono = findViewById(R.id.etTelefono);
        email = findViewById(R.id.etEmail);

        probar = findViewById(R.id.btnProbar);

        probar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String valor = nombre.getText().toString();
                if (valor.equals("Marta")) {
                    Log.e("PRUEBAS", "Esta vacio el campo");
                }else{
                    Log.e("PRUEBAS",nombre.getText().toString());
                    nombre.setText("");
                }

            }
        });

        titulo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("PRUEBA2", titulo.getText().toString());
            }
        });

        todos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ancho.setText("Ya vamos por la mitad de la TEORIA");
            }
        });

        loremIpsum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("PUEBAS3", String.valueOf(loremIpsum.getLineCount()) );
            }
        });

        fondo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("PUEBAS3", String.valueOf(fondo.length()) );
            }
        });

        ancho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tituloSuperiorIzquierdo.setVisibility(View.GONE);
            }
        });





    }
}